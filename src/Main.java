import facade.FileConversionFacade;
import model.Email;
import observer.listeners.EmailClient;

import java.util.HashMap;
import java.util.Map;

public class Main {

    public static void main(String args []) {
        System.out.println("*** Email system ***");

        EmailClient clientMario = new EmailClient("mario@acme.net", "<<La Paz, Bolivia>> - <<7198580>>");
        EmailClient clientGeovanna = new EmailClient("geovanna@acme.net");
        EmailClient clientMarcelo = new EmailClient("marcelo@acme.net");
        EmailClient clientGabriel = new EmailClient("gabriel@acme.net");
        EmailClient clientDaniel = new EmailClient("daniel@acme.net");
        EmailClient clientLuis = new EmailClient("luis@acme.net", "<<Cochabamba, Bolivia>> - <<71989858>>");

        Map<String, EmailClient> clientsMap = new HashMap<>();
        clientsMap.put("mario@acme.net", clientMario);
        clientsMap.put("geovanna@acme.net", clientGeovanna);
        clientsMap.put("marcelo@acme.net", clientMarcelo);
        clientsMap.put("gabriel@acme.net", clientGabriel);
        clientsMap.put("daniel@acme.net", clientDaniel);
        clientsMap.put("luis@acme.net", clientLuis);

        // Observer
        EmailServer emailServer = new EmailServer();
        emailServer.events.subscribe("Receiver", clientMario);
        clientsMap.forEach( (k,v) -> {
            emailServer.events.subscribe("Receiver", v);
        } );

        // Facade
        FileConversionFacade conversionFacade = new FileConversionFacade();

        Email email1 = new Email();
        email1.setTo("marcelo@acme.net");
        email1.setSubject("Acciones contra smells");
        email1.setBody("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquid ex ea commodi consequat. Quis aute iure reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint obcaecat cupiditat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.");
        email1.setAttachFile(conversionFacade.encodeFile("documento.doc", "UTF"));

        Email email2 = new Email();
        email2.setTo("mario@acme.net");
        email2.setSubject("Sobre Robert C. Martin");
        email2.setBody("Robert Cecil Martin (n. 1952, coloquialmente conocido como Uncle Bob) es un ingeniero de software y autor estadounidense, reconocido por desarrollar varios principios de diseño de software y ser uno de los coautores del Manifiesto Ágil. Martin es autor de varios artículos y libros. Fue el editor de la revista C++ Report y primer director de la Agile Alliance.");

        // Envio de emails
        EmailTransmisionChanel.send(emailServer, clientMario, email1, clientsMap);
        EmailTransmisionChanel.send(emailServer, clientLuis, email2, clientsMap);
        EmailTransmisionChanel.send(emailServer, clientGabriel, email2, clientsMap);
        EmailTransmisionChanel.send(emailServer, clientGeovanna, email2, clientsMap);

        // Vistas de clientes email
        EmailClientDisplay.display(clientMario);
        EmailClientDisplay.display(clientGeovanna);
        EmailClientDisplay.display(clientMarcelo);
        EmailClientDisplay.display(clientGabriel);
        EmailClientDisplay.display(clientDaniel);
        EmailClientDisplay.display(clientLuis);

    }
}
