import composite.component.EmailComponent;
import composite.leaf.Leaf;
import model.Email;
import observer.listeners.EmailClient;

import java.util.Map;

public class EmailTransmisionChanel {
    public static void send(EmailServer server, EmailClient clientFrom, Email email, Map<String, EmailClient> clients) {
        Email finalEmail = clientFrom.setDataForSend(email);

        // Observer
        server.emailReceived(finalEmail);

        // Composite
        EmailComponent emailLeaf = new Leaf(finalEmail);
        EmailClient clientTo = clients.get(email.getTo());
        clientTo.inbox.addFileComponent(emailLeaf);
        clientFrom.sent.addFileComponent(emailLeaf);
    }
}
