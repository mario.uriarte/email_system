package model;

import java.io.File;

public class Email {
    protected String from;
    protected String sent;
    protected String to;
    protected String subject;
    protected String cc;
    protected String body;
    protected String signature;
    protected String text;
    protected File attachFile;


    public File getAttachFile() {
        return attachFile;
    }

    public void setAttachFile(File attachFile) {
        this.attachFile = attachFile;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getSent() {
        return sent;
    }

    public void setSent(String sent) {
        this.sent = sent;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getCc() {
        return cc;
    }

    public void setCc(String cc) {
        this.cc = cc;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void show() {
        System.out.println("Email from: " + this.from);
        System.out.println("To: " + this.to);
        System.out.println("Subject: " + this.subject + "\n");
        System.out.println(this.body + "\n");

        if (this.attachFile != null) {
            System.out.println("One file attach\n");
        }

        if(this.signature != null) {
            System.out.println(this.signature + "\n");
        }
    }
}
