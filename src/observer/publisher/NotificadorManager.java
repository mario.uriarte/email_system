package observer.publisher;

import model.Email;
import observer.listeners.EventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NotificadorManager {

    Map<String, List<EventListener>> listeners = new HashMap<>();

    public NotificadorManager(String... operations) {
        for (String operation : operations) {
            this.listeners.put(operation, new ArrayList<>());
        }
    }

    public void subscribe(String eventType, EventListener listener) {
        List<EventListener> users = listeners.get(eventType);
        users.add(listener);
    }

    public void unsubscribe(String eventType, EventListener listener) {
        List<EventListener> users = listeners.get(eventType);
        users.remove(listener);
    }

    public void notifySubscribers(String eventType, Email email) {
        List<EventListener> users = listeners.get(eventType);

        for (EventListener listener : users) {

            if (email.getFrom().equals(listener.getAddress())) {
                System.out.println("---------------------------------------");
                System.out.println("> Incoming message to " + email.getFrom());
                listener.receive(eventType, email);

            }

        }
    }
}
