package observer.listeners;

import composite.component.EmailComponent;
import composite.composite.CompoundFolder;
import composite.leaf.Leaf;
import model.Email;

public class EmailClient implements EventListener {
    String address;
    String signature;

    // folders
    public CompoundFolder inbox;
    public CompoundFolder sent;
    public CompoundFolder draft;
    public CompoundFolder trash;


    public EmailClient(String address, String signature) {
        this.address = address;
        this.signature = signature;
        initFolders();
    }

    public EmailClient(String address) {
        this.address = address;
        initFolders();
    }

    @Override
    public String getAddress() {
        return address;
    }

    @Override
    public void receive(String eventType, Email email) {
        email.show();
    }

    public Email setDataForSend(Email email) {
        email.setFrom(this.address);
        if (this.signature != null) {
            email.setSignature(this.signature);
        }

        return email;
    }

    private void initFolders() {
        // Composite
        this.inbox = new CompoundFolder("Inbox");
        this.sent = new CompoundFolder("Sent");
        this.draft = new CompoundFolder("Draft");
        this.trash = new CompoundFolder("Trash");

        this.populateEmails(5);
    }

    private void populateEmails(int amount) {

        for ( int i = 0; i < amount; i++ ) {
            Email email = new Email();
            email.setFrom("rick.sanchez@acme.com");
            email.setTo(address);
            email.setSubject((i + 1) + " Neque porro quisquam est qui dolorem ipsum quia dolor sit amet");
            email.setBody("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquid ex ea commodi consequat. Quis aute iure reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint obcaecat cupiditat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.");

            EmailComponent emailLeaf = new Leaf(email);
            this.inbox.addFileComponent(emailLeaf);
        }

    }
}
