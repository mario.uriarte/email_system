package observer.listeners;

import model.Email;

public interface EventListener {
    void receive(String eventType, Email email);
    public String getAddress();
}
