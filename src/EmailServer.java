import model.Email;
import observer.listeners.EmailClient;
import observer.publisher.NotificadorManager;

public class EmailServer {
    public NotificadorManager events;

    public EmailServer() {
        this.events = new NotificadorManager("Receiver");
    }

    public void emailReceived(Email email) {
        events.notifySubscribers("Receiver", email);
    }
}
