import observer.listeners.EmailClient;

public class EmailClientDisplay {

    public static void display(EmailClient client) {
        System.out.println("==========================================================");
        System.out.println("          *** EMAIL CLIENT " + client.getAddress() + " ***");
        showInbox(client);
        showSent(client);
        showDraft(client);
        showTrash(client);
    }

    public static void show(EmailClient client) {
        System.out.println("---------------------------------------");
        System.out.println("Address " + client.getAddress());
    }

    public static void showInbox(EmailClient client) {
        show(client);
        client.inbox.display(0);
    }

    public static void showSent(EmailClient client) {
        show(client);
        client.sent.display(0);
    }

    public static void showDraft(EmailClient client) {
        show(client);
        client.draft.display(0);
    }

    public static void showTrash(EmailClient client) {
        show(client);
        client.trash.display(0);
    }
}
