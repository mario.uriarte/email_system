package composite.component;

public interface EmailComponent {

    public String getName();
    String getIndentation(int indent);
    public void display(int indent);
}
