package composite.composite;

import composite.component.EmailComponent;

import java.util.ArrayList;
import java.util.List;

public class CompoundFolder implements EmailComponent {

    protected String name;
    private List<EmailComponent> subEmailComponent;

    public CompoundFolder(String name) {
        this.name = name;
        this.subEmailComponent = new ArrayList<EmailComponent>();
    }

    @Override
    public String getName() {
        return this.name;
    }

    public void addFileComponent(EmailComponent fileComponent) {
        this.subEmailComponent.add(fileComponent);
    }

    public String getIndentation(int indent) {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < indent; i++) {
            sb.append("    ");
        }
        return sb.toString();
    }

    @Override
    public void display(int indent) {
        System.out.println(this.getIndentation(indent) + this.name);

        for (EmailComponent ec : this.subEmailComponent) {
            ec.display(indent + 1);
        }
    }

}
