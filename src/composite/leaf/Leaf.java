package composite.leaf;

import composite.component.EmailComponent;
import model.Email;

public class Leaf extends Email implements EmailComponent {

    public Leaf(Email email) {
        this.from = email.getFrom();
        this.to = email.getTo();
        this.subject = email.getSubject();
    }

    @Override
    public String getName() {
        return "From: " + this.from + ", To: " + this.to + ", Subject: " + this.subject;
    }

    @Override
    public String getIndentation(int indent) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < indent; i++) {
            sb.append("    ");
        }
        return sb.toString();
    }

    @Override
    public void display(int indent) {
        System.out.println(
                this.getIndentation(indent) + this.getName());
    }
}
