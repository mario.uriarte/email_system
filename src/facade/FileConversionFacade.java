package facade;

import java.io.File;

public class FileConversionFacade {

    public File encodeFile(String fileName, String format) {

        System.out.println("FileConversionFacade: encode started...");
        AttachFile file = new AttachFile(fileName);
        Codec sourceCodec = CodecFactory.encode(file, format);

        File result = (new NormalizeFile()).fix(file, sourceCodec);

        System.out.println("FileEncodeFacade: conversion completed.");
        return result;
    }
}