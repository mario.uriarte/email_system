package facade;

public class CodecFactory {

    public static Codec encode(AttachFile file, String type) {

        if (type.equals("UTF")) {
            System.out.println("CodecFactory: encode UTF...");
            return new UTFcodec();
        } else {
            System.out.println("CodecFactory: encode Base64...");
            return new Base64codec();
        }
    }

}
