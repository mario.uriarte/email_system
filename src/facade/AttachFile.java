package facade;

public class AttachFile {
    private String name;

    public AttachFile(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
